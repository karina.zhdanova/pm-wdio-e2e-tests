/**
 * Class contains common short registration with email page logic
 * @extends RegistrationPage
 */

import RegistrationPage from "./Registration.page";

class RegMailPage extends RegistrationPage {
    
    get emailInput() {
        return $("input#email");
    }

    get shortSuccessPanel(){
        return $(".short-registration-success");
    }

    registerMail(email, day, month, year, currency , password){
        this.emailInput.waitForVisible();
        this.emailInput.setValue(email);
        this.birthDay.selectByValue(day);
        this.birth_month.selectByValue(month);
        this.birth_year.selectByValue(year);
        this.currency.selectByValue(currency);
        this.password.setValue(password);

    }

    waitShortSuccessPageToLoad() {
        if (!this.shortSuccessPanel.isVisible()) {
            this.shortSuccessPanel.waitForVisible();
        }
    }

    clickRegister(){
        this.registration_button.waitForVisible();
        this.registration_button.click();
    }
}

export default RegMailPage;