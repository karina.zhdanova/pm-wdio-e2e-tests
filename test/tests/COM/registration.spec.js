import RegistrationPage from "../pages/Registration.page.js";
let userData = require('../fixtures/user.json');
//let invalid_email_user = require('../fixtures/user_invalid_email.json');
const registrationPage = new RegistrationPage();
const email = `test${Date.now()}@gmail.com`;
const phone = Math.floor(Math.random() * 9000000000) + 1000000000;

describe.skip('registation form ', function () {
    beforeEach(function () {

        browser.url('/#registration');
        /*browser.setViewportSize({
            width: 600,
            height: 1000
        });*/
    });

    it('C9807 should let your register with proper credentials ', function () {
        
        registrationPage.setNameInfo(
            userData.name,
            userData.surname
        );
        registrationPage.setBirthDay(
            userData.birth_day,
            userData.birth_month, userData.birth_year,
        );
        registrationPage.setEmail(email, email);

        registrationPage.setAddress(
            userData.country,
            userData.city,
            //userData.zip,
            phone,
            userData.currency
        )
        registrationPage.setPassword(
            userData.password,
            userData.confirm_password,
            userData.question,
            userData.answer
        );

        
        registrationPage.clickRegister();
        //browser.debug();

        registrationPage.waitForSuccessPageToLoad();

        //expect(browser.getUrl().includes('#registration-success')).to.be.true;



    });

});