import HeaderPage from "./../components/header.page";

/**
 * Class contains common login page logic for COM brand
 */

class LoginPage {

    get HeaderPage() {
        return new HeaderPage();
      }

    get login() {
        return $('#login');
    }

    get password() {
        return $('#password');
    }
    

    get loginForm() {
        return $('login-form.login-wrapper ')
    }

    get loginButton() {
        return $('.btn.btn-box.btn-white');
    }

    get loginInfoPanel() {
        return $('.user-deposit');
    }

    get notificationText() {
        return $('notification> div.alert.undefined.global')
    }

    get formAlert() {
        return $('div.form-error.alert');
    }
//
    get buttonLogin() {
        return $('button.btn-blue');
    }
    get forgotLink(){
        this.buttonLogin.click();
        this.login.waitForVisible();
    }

    open() {
        browser.url('/#login');
    }

    /**
     * @param {string} password - password of existed user 
     */
    openLogin() {
        this.buttonLogin.waitForVisible();
        this.buttonLogin.click();
        this.login.waitForVisible();
    }
    
    /**
     * @param {string} login - login (account id)of existed user 
     * @param {string} password - password of existed user 
     */
    loginWithID(login, password) {
        this.login.waitForVisible();
        this.login.setValue(login);
        this.password.setValue(password);
        browser.pause(500);
        this.loginButton.click();

    }

     /**
     * @param {string} email - email of existed user 
     * @param {string} password - password of existed user 
     * 
     */
    loginWithEmail(email, password) {
        this.login.waitForVisible();
        this.login.setValue(email);
        this.password.setValue(password);
        this.loginButton.click();

    }

    /**
     * method waits for login form to load 
     */
    waitForLoginPageToLoad(){
        if (!this.loginForm.isVisible()) {
            this.loginForm.waitForVisible();
        }
    }

   /* waitForDepositPageToLoad() {
        if (!this.logIn_tool_info_panel.isVisible()) {
            this.logIn_tool_info_panel.waitForVisible();
        }
    }*/

    isWrongData() {
        this.notification_text.waitForExist();
        expect(this.notification_text.waitForExist()).to.be.true;
    }

    isLoggedIn() {
        return this.logIn_tool_info_panel.waitForVisible();  
    }

}

export default LoginPage;