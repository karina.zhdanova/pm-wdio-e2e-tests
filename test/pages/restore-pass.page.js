class  RestorePage {

    get account_number(){
        return $('#accountNumber');
    }

    get email(){
        return $('#email');
    }

    get restore_button(){
        return $('.btn.btn-box.btn-orange');
    }
    
    get restore_form(){
        return $('.restore-password');
    }
    restorePassword(acc_num, email){

        this.account_number.waitForVisible();
        this.account_number.setValue(acc_num);
        this.email.setValue(email);
        this.restore_button.click();
    }
    
    waitForSuccessPageToLoad() {
        if (!this.restore_form.isVisible()) {
            this.restore_form.waitForVisible(10000);
        }
    }
        

}

export default RestorePage;