/**
 * Class contains side menu page logic
 */

class SideMenu {

    
    get mainPageMenu() {
        return $('#container > sidebar > div:nth-child(2) > nav > a:nth-child(1)');
    }

    get sidebarOverlay() {
        return $('.sidebar-overlay.show');
    }

    get logoutButton() {
        return $('i.icon.icon-ai-exit');
    }

    get shadowOnTheRight(){
        return $('.sidebar-overlay.show');
    }

    get iosDownload(){
        return $('#container > sidebar > div:nth-child(6) > nav > a:nth-child(2)');
    }

    get androidDownload(){
        return $('#container > sidebar > div:nth-child(6) > nav > a:nth-child(1)');
    }


    openSideMenu() {
        this.burger_button.waitForVisible();
        this.burger_button.click();
        //this.sidebar_overlay.waitForVisible(5000);

    }

    openAllBets() {
        this.mainPageMenu.waitForVisible();
        this.mainPageMenu.click();
    }

    logOut() {
        //browser.moveToObject(logoutButton);
        this.logoutButton.scroll();
        this.logoutButton.waitForVisible();
        this.logoutButton.click();

    }

    openIOS() {
        this.iosDownload.click();

    }

}
export default SideMenu;