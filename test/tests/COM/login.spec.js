import LoginPage from "../pages/Login.page.js";
import MainMenu from "../pages/main-menu.page.js";
import WalkthroughPage from "../pages/walkthrough.page.js";

const loginPage = new LoginPage();
const mainMenu = new MainMenu();
const walkPage = new WalkthroughPage();

describe.skip("Login Page", function() {
  const validId = "5357920";
  const validPass = "qweASD123";
  const validEmail = "bet.gel.ge.rich@gmail.com";
  const validEMailPass = "qweASD123";

  beforeEach(function() {
    browser.url("/#login");

    /*browser.setViewportSize({
            width: 1600,
            height: 1000
        });*/
    //browser.windowHandleFullscreen();
  });

  it("C9802 should deny access with wrong creds", function() {
    //walkPage.waitForLoginAction();
    //walkPage.skipSlides();
    //loginPage.openLogin();
    //loginPage.waitForLoginPageToLoad();

    loginPage.loginWithID("bla", "123456!Aa");
    loginPage.login_button.click();
    expect(loginPage.notification_text.getText()).to.contain(
      "Email / Phone / Account number is incorrect"
    );
    expect(loginPage.isLoggedIn()).to.be.false;
  });

  it("should show error acess on missing password", function() {
    //loginPage.openLogin();
    loginPage.loginWithID(validId, "");

    expect(loginPage.form_alert.getText()).to.contain("Enter password");
    expect(loginPage.isLoggedIn()).to.be.false;
  });

  it("should show error acess on missing id/email", function() {
    //loginPage.openLogin();
    loginPage.loginWithID("", validPass);

    expect(loginPage.form_alert.getText()).to.contain(
      "Enter Email / Phone / Account number"
    );
    expect(loginPage.isLoggedIn()).to.be.false;
  });

  it("should allow access with correct creds", function() {
    //loginPage.openLogin();
    loginPage.loginWithID(validId, validPass);
    loginPage.waitForDepositPageToLoad();
    expect(loginPage.isLoggedIn()).to.be.true;
  });

  it("should allow access with correct email  creds", function() {
    loginPage.loginWithID(validEmail, validPass);
    loginPage.waitForDepositPageToLoad();
    expect(loginPage.isLoggedIn()).to.be.true;
  });
});
