/**
 * Class contains common header page logic
 */

 class HeaderPage {

    get burgerButton() {
        return $('#sidebar-handle > i');
    }

    get loginButton() {
        return $('.btn.btn-box.btn-white');
    }


 }

 export default HeaderPage;