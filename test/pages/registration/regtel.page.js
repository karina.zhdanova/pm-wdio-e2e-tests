/**
 * Class contains common short registration with phone page logic
 * @extends RegistrationPage
 */
import RegistrationPage from "./Registration.page";

class RegTelPage extends RegistrationPage{
    
    get shortRegForm(){
        return $('short-registration')
    }
    get short_success_panel(){
        return $(".short-registration-success");
    }
    get phone_number() {
        return $("input[name='phoneNumber']");
    }

    get validation_error(){
        return $('.form__error')
    }
    registerTel(phone_number, day, month, year, currency , password){
        this.phone_number.waitForVisible();
        this.phone_number.setValue(phone_number);
        this.birthDay.selectByValue(day);
        this.birth_month.selectByValue(month);
        this.birth_year.selectByValue(year);
        this.currency.selectByValue(currency);
        this.password.setValue(password);

    }

    waitShortRegPagToLoad() {
        if (!this.shortRegForm.isVisible()) {
            this.shortRegForm.waitForVisible(10000);
        }
    }

    waitShortSuccessPageToLoad() {
        if (!this.short_success_panel.isVisible()) {
            this.short_success_panel.waitForVisible(10000);
        }
    }
   
    clickRegister(){
        this.registration_button.waitForVisible();
        this.registration_button.scroll();
        this.registration_button.click();
    }
}

export default RegTelPage;