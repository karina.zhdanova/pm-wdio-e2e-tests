import LoginPage from "../pages/Login.page.js";
import MainMenu from "../pages/main-menu.page.js";
import WalkthroughPage from "../pages/walkthrough.page.js";
import RegTelPage from "../pages/regtel.page.js";

const walkPage = new WalkthroughPage();
const loginPage = new LoginPage();
//const registrationPage = new RegistrationPage();
const regtelPage = new RegTelPage();

describe.skip("Walkthrough tutorial", function() {
  browser.addCommand("clearEverything", function() {
    browser.deleteCookie();
    browser.execute(function() {
      window.localStorage.clear();
      window.sessionStorage.clear();
    });
  });

  beforeEach(function() {
    browser.url("");
    /*browser.setViewportSize({
        width: 480,
        height: 1100*/
    // browser.windowHandleSize({width: 800, height: 600});
  });

  //browser.windowHandleFullscreen();

  afterEach(function() {
    browser.clearEverything();
  });

  it("should open walkthrough page", function() {
    //walkPage.waitForLoginAction();
    expect(browser.getUrl().includes("walkthrough")).to.be.true;
  });

  it(
    "should open login form ",
    function() {
      walkPage.waitForLoginAction();
      walkPage.openLoginPage();
      loginPage.waitForLoginPageToLoad();
      expect(loginPage.loginForm.isVisible()).to.be.true;
    },
    3
  );

  it("should open registration form", function() {
    walkPage.openRegistrationPage();
  });
});
