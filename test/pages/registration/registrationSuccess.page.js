/**
 * Class contains common elements for Registration success page 
 * after user is successfully registered 
 */
class RegistrationSuccess {

    //UI elements 
    get userID(){
        return $('.short-registration-success__number').getText().trim();
    }

    get confirmPhoneButton(){
        return $('.short-registration-success__confirm-button');
    }
}

export default RegistrationSuccess;