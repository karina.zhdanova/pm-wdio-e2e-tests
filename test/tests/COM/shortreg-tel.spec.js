import faker from 'faker';
import RegTelPage from "../pages/regtel.page.js";
let regTelPage = new RegTelPage();

describe.only('short registration with phone form ', function () {

    beforeEach(function () {

        browser.url('/#regtel');
        /*browser.setViewportSize({
            width: 480,
            height: 1000
        });*/
    
        //browser.windowHandleFullscreen();  Enter correct number +xxxxxxxxxx
        
    });

    

    it('С9805 register user with letters phone number ', function(){
    
        regTelPage.registerTel(
            user.lettersPhone,
            user.day,
            user.month,
            user.year,
            user.currency,
            user.password
        );

        //regTelPage.clickRegister();
        expect(regTelPage.validation_error.getText()).to.contain('Phone number must contain only digits');
        //regTelPage.waitShortSuccessPageToLoad();

        //expect(browser.getUrl().includes('#registration-success-tel')).to.be.true;
    });


    it('С9805 register user with invalid phone number ', function(){
    
        regTelPage.registerTel(
            user.invalidPhone,
            user.day,
            user.month,
            user.year,
            user.currency,
            user.password
        );

       // regTelPage.clickRegister();
        expect(regTelPage.validation_error.getText()).to.contain('Enter correct number +xxxxxxxxxx');
        //regTelPage.waitShortSuccessPageToLoad();

        //expect(browser.getUrl().includes('#registration-success-tel')).to.be.true;
    });

    it('С9805 register user with valid phone number ', function(){
    
        regTelPage.registerTel(
            user.phone,
            user.day,
            user.month,
            user.year,
            user.currency,
            user.password
        );

        regTelPage.clickRegister();
        regTelPage.waitShortSuccessPageToLoad();

        expect(browser.getUrl().includes('#registration-success-tel')).to.be.true;
    });

}); 