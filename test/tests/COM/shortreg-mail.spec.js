import faker from 'faker';
import RegMailPage from "../pages/regmail.page.js";
let regMailPage = new RegMailPage();

const user = {
    mail : faker.internet.email(),
    invalidEmail : '34567',
    day :"25" ,
    month: "04",
    year:"1992" ,
    currency: "1",
    password : '123Aaa!*'

}

describe.skip('short registration with email  form ', function () {

    beforeEach(function () {

        browser.url('/#regmail');
        browser.setViewportSize({
            width: 480,
            height: 760
        });
    
        //browser.windowHandleFullscreen();
        
    });

    it('C9806 register user with valid email ', function(){
    
        regMailPage.registerMail(
            user.mail,
            user.day,
            user.month,
            user.year,
            user.currency,
            user.password
        );

        regMailPage.clickRegister();
        regMailPage.waitShortSuccessPageToLoad();

        expect(browser.getUrl().includes('#registration-success-mail')).to.be.true;
    })

    it('register user with invalid email ', function(){
    
        regTelPage.registerTel(
            user.invalidEmail,
            user.day,
            user.month,
            user.year,
            user.currency,
            user.password
        );

        regTelPage.clickRegister();
        expect(regTelPage.validation_error.getText()).to.contain('Enter valid email');
        
    });
}) 