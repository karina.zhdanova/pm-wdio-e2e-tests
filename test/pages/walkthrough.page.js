/**
 * Class contains logic for Walkthrough tutorial
 **/
class WalkthroughPage {
 // UI elements 
  get walkthrourghLogin() {
    return $(".walkthrough__action").$("button.btn.btn-black");
  }

  get walkthroughActions() {
    return $(".walkthrough__action");
  }

  get walkthrourghJoin() {
    return $("button.btn.btn-orange");
  }

  get walkthroughSkip() {
    return $(".walkthrough__skip");
  }

  /**
   * Clicks on Login button and opens the Login form 
   */
  openLoginPage() {
    this.walkthrourghLogin.click();
    //browser.timeouts('implicit', 5000);
  }

  /**
   * Clicks on Join Now button and opens the Login form 
   */
  openRegistrationPage() {
    this.walkthrourghJoin.click();
    //browser.timeouts('implicit', 5000);
  }

  /**
   * Clicks on Skip link button and opens 
   */
  skipSlides() {
    this.skip.waitForVisible();
    this.skip.click();
  }

  waitForLoginAction() {
    if (!this.walkthroughActions.isVisible()) {
      this.walkthroughActions.waitForVisible();
    }
  }
}

export default WalkthroughPage;
