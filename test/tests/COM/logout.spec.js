import MainMenu from "../pages/main-menu.page";
import LoginPage from "../pages/Login.page.js";
import WalkthroughPage from "../pages/walkthrough.page.js";

const loginPage = new LoginPage();
const mainMenu = new MainMenu();
const walkPage = new WalkthroughPage();

describe.skip('logout flow', function () {

    beforeEach(function () {

        browser.url('/#login');
        browser.setViewportSize({
            width: 480,
            height: 760
        });
    
        walkPage.openLoginPage();
        loginPage.waitForLoginPageToLoad();

        loginPage.loginWithID('1400739', 'qweASD123');
        loginPage.waitForregisterPageToLoad();
    });

    it('С9804 user should be logged out', function () {
        
        
        mainMenu.openMenu();
        mainMenu.logOut();
        expect(browser.getUrl().includes('#deposit')).to.be.true;

    })

}); 