#Selenium WebdriverIO  with Mocha + Chai 


##Tech Stack
-------------
node ( Version 6^ ) <https://nodejs.org>

selenium-standalone <https://github.com/vvo/selenium-standalone>

webdriverio <https://github.com/webdriverio/webdriverio/>

Mocha <https://mochajs.org/>

Chai <https://chaijs.com> ( test are written in Chai with es6 )

Sinon <https://sinonjs.org> ( generates random units for testing )


##Installation
-------------
`npm install`

###Note: this installation requires Node 6^ , NPM 3^ , & Java 1.8^
##Common Errors:
- `cant connect to selenium`, it might be due to vagrant or another virtual machine. run `node run kill-selenium` to kill all machines running on port 4444.
- `Error: Missing /usr/local/lib/node_modules/selenium-standalone/.selenium/chromedriver/XXX`. goto http://chromedriver.chromium.org/ and install the latest driver.

##Run Tests
-------------
To run tests, simply run `npm test`.

note: `npm test`, runs all test within the test directory.
To kill/stop all selenium instances use: `npm run kill-selenium`.
use `npm run help` to see webdriverio's testrunner commands.
Also, when the selenium-standalone server starts, it' running on port 4444

##Generate Allure report locally
-------------

Install the Allure command-line tool, and process the results directory:

`allure generate [allure_output_dir] && allure open`
This will generate a report (by default in ./allure-report), and open it in your browser

To generate Allure report run

`npm run report`
This will generate a report (by default in ./allure-report), and open it in your browser



##Notes
-------------

while developing note that `Class browser.whatever()` uses all the methods here: <http://webdriver.io/api.html>

For configuration changes, please read & refer to the `wdio.config.js` file. All the information on how to configure this can be found on webdriverio <http://webdriver.io/guide/testrunner/configurationfile.html>

The global scope for all tests is located in the `before` method inside of the wdio.config.js file.
So, if you have any vars you want to declare for all your test to have access, you can put them there.

`before: function (capabilities, specs) {
  ...
}
`
##Debug
-------------

In order to debug run `DEBUG=true npm test`