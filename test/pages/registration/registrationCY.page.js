/**
 * Class contains common registration page logic for CY brand
 */

class CYRegistrationPage extends RegistrationPage{

    get nameField() {
        return $('input#firstName');
    }

    get surnameField() {
        return $('#lastName');
    }

    get birthDay() {
        return $("select[name='day']");
    }

    get birthMonth() {
        return $("select[name='month']");
    }

    get birthYear() {
        return $("select[name='year']");
    }


    get emailField() {
        return $('#email');
    }

    get confirmEmail() {
        return $('#confirmEmail');
    }

    get selectCountry() {
        return $('#countryId');
    }

    get cityField() {
        return $('#city');
    }

    get phoneField() {
        return $('#phone');
    }

    get currencyField() {
        return $('#currencyId');
    }

    get passwordField() {
        return $('#password');
    }

    get confirmPasswordField() {
        return $('#confirmPassword');
    }

    get securityQuestion() {
        return $('#securityQuestion');
    }

    get securityAnswer() {
        return $('#securityAnswer');
    }

    get checkbox() {
        return $('.switcher');
    }
    get registrationButton() {
        return $('.btn.btn-box.btn-orange');
    }

    get registrationButton() {
        return $('registration-form.registration');
    }
    get register_info_panel() {
        return $('.registration-success')
    }


    setNameInfo(name, surname) {
        this.registrationButton.waitForVisible();
        this.nameField.waitForVisible();
        this.nameField.setValue(name);
        this.surnameField.setValue(surname);
    }
    setBirthDay(day, month, year) {
        this.birthDay.selectByValue(day);
        this.birthMonth.selectByValue(month);
        this.birthYear.selectByValue(year);
    }

    setEmail(email, confirmEmail) {
        this.email.setValue(email);
        this.confirmEmail.setValue(confirmEmail);
    }

    setAddress(country, city, phone, currency) {
        this.select_country.selectByValue(country);
        this.city.setValue(city);
        //this.address.setValue(address);
        // this.zip.setValue(zip);
        this.phone.setValue(phone);
        this.currency.selectByValue(currency);
    }

    setPassword(password, confirmPassword, securityQuestion, securityAnswer) {
        this.password.setValue(password);
        this.confirm_password.setValue(password);
        this.securityQuestion.selectByValue(securityQuestion);
        this.securityAnswer.setValue(securityAnswer);

    }
    clickRegister() {
    let checkbox = $('.switcher')
    checkbox.click()
    //this.registrationButton.scroll();  
    this.registrationAnswer.click()

    //this.checkbox.click();
    //this.registrationButton.waitForVisible();
    //this.registrationButton.click();
        //expect(this.registrationButton.waitForVisible()).to.be.false;

    }

    waitForSuccessPageToLoad() {
        if (!this.register_info_panel.isVisible()) {
            this.register_info_panel.waitForVisible(10000);
        }
    }


    isRegistered() {
        this.register_info_panel.waitForExist();
        browser.getUrl().includes('#registration-success');

    }
}

export default RegistrationPage;