/**
 * Class contains common header page logic for COM brand
 */

class RegistrationPage {

    get name() {
        return $('input#firstName');
    }

    get surname() {
        return $('#lastName');
    }

    get birthDay() {
        return $("select[name='day']");
    }

    get birthMonth() {
        return $("select[name='month']");
    }

    get birthYear() {
        return $("select[name='year']");
    }

    get email() {
        return $('#email');
    }

    get confirmEmail() {
        return $('#confirmEmail');
    }

    get selectCountry() {
        return $('#countryId');
    }

    get city() {
        return $('#city');
    }

    get phone() {
        return $('#phone');
    }

    get currency() {
        return $('#currencyId');
    }

    get password() {
        return $('#password');
    }

    get confirmPassword() {
        return $('#confirmPassword');
    }

    get securityQuestion() {
        return $('#securityQuestion');
    }

    get securityAnswer() {
        return $('#securityAnswer');
    }

    get checkbox() {
        return $('.switcher');
    }
    get registrationButton() {
        return $('.btn.btn-box.btn-orange');
    }registrationButton

    get registrationForm() {
        return $('registration-form.registration');
    }
    get registerInfoPanel() {
        return $('.registration-success')
    }


    
    setNameInfo(name, surname) {
        this.registrationForm.waitForVisible();
        this.name.waitForVisible();
        this.name.setValue(name);
        this.surname.setValue(surname);
    
    setBirthDbirthDayonth, year) {
        this.birth_day.selectByValue(day);
        this.birth_month.selectByValue(month);
        this.birth_year.selectByValue(year);
    }

    setEmail(email, confirmEmail) {
        this.email.setValue(email);
        this.confirm_email.setValue(confirmEmail);
    }

    setAddress(country, city, phone, currency) {
        this.select_country.selectByValue(country);
        this.city.setValue(city);
        //this.address.setValue(address);
        // this.zip.setValue(zip);
        this.phone.setValue(phone);
        this.currency.selectByValue(currency);
    }

    setPassword(password, confirmPassword, security_question, security_answer) {
        this.password.setValue(password);
        this.confirmPassword.setValue(password);
        this.security_question.selectByValue(security_question);
        this.security_answer.setValue(security_answer);

    }
    clickRegister() {
    let checkbox = $('.switcher')
    checkbox.click()
    //this.registration_button.scroll();  
    this.registration_button.click()

    //this.checkbox.click();
    //this.registration_button.waitForVisible();
    //this.registration_button.click();
        //expect(this.registration_button.waitForVisible()).to.be.false;

    }

    waitForSuccessPageToLoad() {
        if (!this.register_info_panel.isVisible()) {
            this.register_info_panel.waitForVisible(10000);
        }
    }


    isRegistered() {
        this.register_info_panel.waitForExist();
        browser.getUrl().includes('#registration-success');

    }
}

export default RegistrationPage;