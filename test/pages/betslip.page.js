class BetSlip {

    get singleTab(){
        return $('#header-tab-express');
    }

    get expressTab(){
        return $('#header-tab-single');
    }

    get systemTab(){
        return $('#header-tab-system');
    }

    get placeBetButton(){
        return $('div.betslip-footer  .btn.btn-orange');
    }

    get betSum(){
        return $('.stake-value__input');
    }

    get crossButton(){
        return $('.btn.bet__close');
    }

    
}