/**
 * Class contains tabbar menu page logic
 */

 class TabBar {

    get sportsTab(){
        return $('tabbar > nav > a:nth-child(1)');
    } 

    get liveBettingTab(){
        return $('tabbar > nav > a:nth-child(2)');
    }

    get favoritesTab(){
        return $('tabbar > nav > a:nth-child(3)');
    }

    get betsTab(){
        return $('tabbar > nav > a:nth-child(4)');
    }

    get gamesTab(){
        return $('tabbar > nav > a:nth-child(5)');
    }
 }

 export default TabBar;