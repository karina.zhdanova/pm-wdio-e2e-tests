import RestorePage from "../pages/restore-pass.page";

const restorePage = new RestorePage();
describe.skip('restore password form ', function () {
    beforeEach(function () {

        browser.url('/#restore-password');
        browser.setViewportSize({
            width: 480,
            height: 760
        });
    
        
    });

    it('С9812 restore password for registered user by email and account id  ', function () {
        
        restorePage.restorePassword('4303582', 'karina.zhd@gmail.com');
        restorePage.waitForSuccessPageToLoad();
        
        
     } );

    it('should show error for empty credentials', function(){

    });

    it('should show error for incorrect email ', function(){

    });

    it('should show error for incorrect id ', function(){

    })
    }); 
